<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 21/03/17
 * Time: 0:17
 */

namespace AppBundle\Util;


class Slugger
{
    // Código copiado de http://cubiq.org/the-perfect-php-clean-url-generator
    static public function getSlug($cadena, $separador = '-')
    {
        $slug = iconv('UTF-8', 'ASCII//TRANSLIT', $cadena);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = strtolower(trim($slug, $separador));
        $slug = preg_replace("/[\/_|+ -]+/", $separador, $slug);
        return $slug;
    }
}