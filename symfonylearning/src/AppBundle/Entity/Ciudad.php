<?php
// src/AppBundle/Entity/Ciudad.php
namespace  AppBundle\Entity;

    use AppBundle\Util\Slugger;
    use Doctrine\ORM\Mapping as ORM;
    /** * @ORM\Entity(repositoryClass="AppBundle\Repository\CiudadRepository") */
    class Ciudad
    {
        /**
         * @ORM\Id
         * @ORM\GeneratedValue
         * @ORM\Column(type="integer")
         */
            protected $id;
        /**
         * @ORM\Column(type="string", length=100)
         */
            protected $nombre;
        /**
         * @ORM\Column(type="string", length=100)
         */
            protected $slug;

        public function getId()
        {
            return $this->id;
        }
        public function setId($id)
        {
            $this->id = $id;
            return $this;
        }
        public function getNombre()
        {
            return $this->nombre;
        }
        public function setNombre($nombre)
        {
            $this->nombre = $nombre;
            $this->slug = Slugger::getSlug($nombre);
            return $this;
        }
        public function getSlug()
        {
            return $this->slug;
        }
        public function setSlug($slug)
        {
            $this->slug = $slug;
            return $this;
        }
        public function __toString()
        {
            return $this->getNombre();
        }


    }