<?php

namespace AppBundle\Entity;

use AppBundle\Util\Slugger;
use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsuarioRepository")
 */
class Usuario
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;
    /**
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;
    /**
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;
    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;
    /**
     * @ORM\Column(name="direccion", type="text")
     */
    private $direccion;
    /**
     * @ORM\Column(name="permiteEmail", type="boolean")
     */
    private $permiteEmail;
    /**
     * @ORM\Column(name="fechaAlta", type="datetimetz")
     */
    private $fechaAlta;
    /**
     * @ORM\Column(name="fechaNacimiento", type="datetime")
     */
    private $fechaNacimiento;
    /**
     * @ORM\Column(name="dni", type="string", length=9)
     */
    private $dni;
    /**
     * @ORM\Column(name="numeroTarjeta", type="string", length=20)
     */
    private $numeroTarjeta;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ciudad")
     */
    private $ciudad;


    public function __construct() {
        $this->fechaAlta = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        $this->slug = Slugger::getSlug($nombre);
        return $this;
    }
    public function getNombre()
    {
        return $this->nombre;
    }
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
    public function setPasswordEnClaro($password)
    {
        $this->password = $password;

        return $this;
    }
    public function getPassword()
    {
        return $this->password;
    }

    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }


    public function getDireccion()
    {
        return $this->direccion;
    }

    public function setPermiteEmail($permiteEmail)
    {
        $this->permiteEmail = $permiteEmail;

        return $this;
    }

    public function getPermiteEmail()
    {
        return $this->permiteEmail;
    }
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }


    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    public function getDni()
    {
        return $this->dni;
    }

    public function setNumeroTarjeta($numeroTarjeta)
    {
        $this->numeroTarjeta = $numeroTarjeta;

        return $this;
    }

    public function getNumeroTarjeta()
    {
        return $this->numeroTarjeta;
    }

    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getCiudad()
    {
        return $this->ciudad;
    }

    public function __toString() {
        return $this->getNombre().' '.$this->getApellidos();
    }



}

