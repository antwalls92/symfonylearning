<?php

namespace AppBundle\Entity;

use AppBundle\Util\Slugger;
use Doctrine\ORM\Mapping as ORM;

/**
 * Oferta
 *
 * @ORM\Table(name="oferta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfertaRepository")
 */
class Oferta
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\Column(name="condiciones", type="text")
     */
    private $condiciones;

    /**
     * @ORM\Column(name="rutaFoto", type="string", length=255)
     */
    private $rutaFoto;

    /**
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=0)
     */
    private $precio;

    /**
     * @ORM\Column(name="descuento", type="decimal", precision=10, scale=0)
     * */
    private $descuento;

    /**
     * @ORM\Column(name="fechaPublicacion", type="datetime")
     */
    private $fechaPublicacion;

    /**
     * @ORM\Column(name="fechaExpiracion", type="datetime")
     */
    private $fechaExpiracion;

    /**
     * @ORM\Column(name="compras", type="integer")
     */
    private $compras;

    /**
     * @ORM\Column(name="umbral", type="integer")
     */
    private $umbral;

    /**
     * @ORM\Column(name="revisada", type="boolean")
     */
    private $revisada;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ciudad")
     */
    private $ciudad;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tienda")
     */
    private $tienda;


    public function getId()
    {
        return $this->id;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        $this->slug = Slugger::getSlug($nombre);
        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }


    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setCondiciones($condiciones)
    {
        $this->condiciones = $condiciones;

        return $this;
    }

    public function getCondiciones()
    {
        return $this->condiciones;
    }

    public function setRutaFoto($rutaFoto)
    {
        $this->rutaFoto = $rutaFoto;

        return $this;
    }

    public function getRutaFoto()
    {
        return $this->rutaFoto;
    }

    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getDescuento()
    {
        return $this->descuento;
    }

    public function setFechaPublicacion($fechaPublicacion)
    {
        $this->fechaPublicacion = $fechaPublicacion;

        return $this;
    }

    public function getFechaPublicacion()
    {
        return $this->fechaPublicacion;
    }

    public function setFechaExpiracion($fechaExpiracion)
    {
        $this->fechaExpiracion = $fechaExpiracion;

        return $this;
    }

    public function getFechaExpiracion()
    {
        return $this->fechaExpiracion;
    }

    public function setCompras($compras)
    {
        $this->compras = $compras;

        return $this;
    }

    public function getCompras()
    {
        return $this->compras;
    }

    public function setUmbral($umbral)
    {
        $this->umbral = $umbral;

        return $this;
    }

    public function getUmbral()
    {
        return $this->umbral;
    }

    public function setRevisada($revisada)
    {
        $this->revisada = $revisada;

        return $this;
    }

    public function getRevisada()
    {
        return $this->revisada;
    }

    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getCiudad()
    {
        return $this->ciudad;
    }

    public function setTienda($tienda)
    {
        $this->tienda = $tienda;

        return $this;
    }

    public function getTienda()
    {
        return $this->tienda;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
}

