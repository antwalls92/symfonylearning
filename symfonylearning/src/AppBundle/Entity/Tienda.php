<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 19/03/17
 * Time: 18:37
 */
// src/AppBundle/Entity/Tienda.php
namespace AppBundle\Entity;

    use AppBundle\Util\Slugger;
    use Doctrine\ORM\Mapping as ORM;

    /** * @ORM\Entity(repositoryClass="AppBundle\Repository\TiendaRepository") */
    class Tienda
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue
        */
        protected $id;
        /**
         * @ORM\Column(type="string", length=100)
         */
        protected $nombre;
        /**
         * @ORM\Column(type="string", length=100)
         */
        protected $slug;
        /**
         * @ORM\Column(type="string", length=10)
         */
        protected $login;
        /**
         * @ORM\Column(type="string")
         */
        protected $password;
        /**
         * @ORM\Column(type="text")
         */
        protected $descripcion;
        /**
         * @ORM\Column(type="text")
         */
        protected $direccion;
        /**
         * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ciudad")
         */
        protected $ciudad;


        public function getId()
        {
            return $this->id;
        }
        public function setId($id)
        {
            $this->id = $id;
            return $this;
        }
        public function getNombre()
        {
            return $this->nombre;
        }
        public function setNombre($nombre)
        {
            $this->nombre = $nombre;
            $this->slug = Slugger::getSlug($nombre);
            return $this;
        }
        public function getSlug()
        {
            return $this->slug;
        }
        public function setSlug($slug)
        {
            $this->slug = $slug;
            return $this;
        }
        public function getLogin()
        {
            return $this->login;
        }
        public function setLogin($login)
        {
            $this->login = $login;
            return $this;
        }
        public function getPassword()
        {
            return $this->password;
        }
        public function setPasswordEnClaro($password)
        {
            $this->password = $password;
            return $this;
        }
        public function setPassword($password)
        {
            $this->password = $password;
            return $this;
        }
        public function getDescripcion()
        {
            return $this->descripcion;
        }
        public function setDescripcion($descripcion)
        {
            $this->descripcion = $descripcion;
            return $this;
        }
        public function getDireccion()
        {
            return $this->direccion;
        }
        public function setDireccion($direccion)
        {
            $this->direccion = $direccion;
            return $this;
        }
        public function getCiudad()
        {
            return $this->ciudad;
        }
        public function setCiudad($ciudad)
        {
            $this->ciudad = $ciudad;
            return $this;
        }
        public function __toString()
        {
            return $this->getNombre();
        }

    }