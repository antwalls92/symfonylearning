<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 20/03/17
 * Time: 23:24
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/** @ORM\Entity(repositoryClass="AppBundle\Repository\VentaRepository") */
class Venta
{
    /**
     * @ORM\Column(type="datetime")
     */
    protected $fecha;
    /**
     *  @ORM\Id
     *  @ORM\ManyToOne(targetEntity="AppBundle\Entity\Oferta")
     */
    protected $oferta;
    /**
     *  @ORM\Id
     *  @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     */
    protected $usuario;


    public function getFecha()
    {
        return $this->fecha;
    }
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
        return $this;
    }
    public function getOferta()
    {
        return $this->oferta;
    }
    public function setOferta($oferta)
    {
        $this->oferta = $oferta;
        return $this;
    }
    public function getUsuario()
    {
        return $this->usuario;
    }
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

}