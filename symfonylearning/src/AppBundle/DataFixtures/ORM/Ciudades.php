<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 21/03/17
 * Time: 0:06
 */
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Ciudad;

class Ciudades extends AbstractFixture implements OrderedFixtureInterface
{

    public function getOrder() { return 1; }
    public function load(ObjectManager $manager)
    {
        $ciudades = array(
            'Jaen',
            'Madrid',
            'Barcelona',
            'Pagalajar'
        );

        foreach ( $ciudades as $ciudad)
        {
            $entidad = new Ciudad();
            $entidad->setNombre($ciudad);
            $manager->persist($entidad);
        }
        $manager->flush();
    }
}