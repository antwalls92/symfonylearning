<?php

namespace AppBundle\Controller;

use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{


    /** @Route(
     *     "/{ciudadId}",
     *      defaults={"ciudadId" = "%app.id_ciudad_por_defecto%"},
     *      name="portada"
     * )
     * @Route("/")
     */
    public function portadaAction($ciudadId )
    {
        if (null === $ciudadId) {
            return $this->redirectToRoute('portada', array(
                'ciudadId' => $this->getParameter('app.ciudad_por_defecto')
            ));
        }

        $em = $this->getDoctrine()->getManager();
        $oferta = $em->getRepository('AppBundle:Oferta')->findOneBy(array(
            'ciudad' => $ciudadId,
            'fechaPublicacion' => new DateTime('today')
        ));

        if(!$oferta)
        {
            throw $this->createNotFoundException("Amiguete, no has creado la oferta del dia!!");
        }
        return $this->render('default/portada.html.twig', array( 'oferta' => $oferta ));

    }


    /** * @Route(
     *     "/sitio/{nombrePagina}/",
     *      requirements={ "nombrePagina"="ayuda|privacidad|sobre_nosotros" },
     *      name="pagina"
     * )
     * */
    public function paginaAction($nombrePagina = 'ayuda') {
        return $this->render('sitio/'.$nombrePagina.'.html.twig');
    }




}
